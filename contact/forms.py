from django import forms
from django.core.mail import send_mail
from autoparts.settings import FEEDBACK_RECIPIENTS
from shop.models import Task


class ContactForm(forms.Form):
    subject = forms.CharField(max_length=100, widget=forms.TextInput(), label='Ваше имя и телефон для связи:')
    vin_num = forms.CharField(max_length=30, widget=forms.TextInput(), label='VIN номер:', required=False)
    message = forms.CharField(widget=forms.Textarea(attrs={'rows': 3}), label='Ваше сообщение:')
    sender = forms.EmailField(widget=forms.TextInput(), label='Ваш e-mail:', required=False)

    def send_mail(self):
        send_mail(
            self.cleaned_data['subject'],
            self.cleaned_data['sender'] + ' прислал вам сообщение на autoshop: ' +
            self.cleaned_data['message'] + ', vin= ' + self.cleaned_data['vin_num'],
            self.cleaned_data['sender'],
            FEEDBACK_RECIPIENTS
        )
        Task.objects.create(
            user=str(self.cleaned_data['subject']),
            vin=str(self.cleaned_data['vin_num']),
            text=str(self.cleaned_data['message']),
            mail=str(self.cleaned_data['sender'])
        )