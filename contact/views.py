from django.http import JsonResponse
from django.views.generic import FormView

from contact.models import Contact
from .forms import ContactForm


class OrderView(FormView):
    template_name = 'main.html'
    form_class = ContactForm

    def form_valid(self, form):
        form.send_mail()
        return JsonResponse({'status': 'Ok',})

    def form_invalid(self, form):
        return JsonResponse({'status': 'error', 'errors': form.errors})

    def get_context_data(self, **kwargs):
        context = super(OrderView, self).get_context_data(**kwargs)
        context['contacts'] = Contact.objects.all()
        return context
