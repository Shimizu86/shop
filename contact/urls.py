from django.conf.urls import url

from contact.views import OrderView
urlpatterns = [
    url(r'^send/', OrderView.as_view(), name='send_order'),
]
