/**
 * Created by marat on 20.07.16.
 */
jQuery(document).ready(function ($) {
    var $callback = $('.fb-feedback');
    $callback.on('click', function () {
        if ($(this).hasClass('open')) {
            $(".global-container-feedback").animate({left: '-320px'}, 500);
            $(".global-container-feedback").animate({left: '0px'}, 500);
            $(".fb-feedback").removeClass('open');
        } else {
            $(".global-container-feedback").animate({left: '0px'}, 500);
            $(".global-container-feedback").animate({left: '320px'}, 500);
            $(".fb-feedback").addClass('open');
        }
    });
});

$(document).ready(function () {
    var content = $('#add-order-form');
    content.submit(function (e) {
        e.preventDefault();
        $.ajax({
            url: "/contact/send/",
            type: 'post',
            data: content.serialize(),
            success: function (data) {
                if (data.status == 'Ok') {
                    $().toastmessage('showSuccessToast', "Ваше сообщение было успешно отправлено");
                }
                if (data.status == 'error') {
                    for (error in data.errors) {
                        console.log(error);
                        if (error == 'message') {
                            $().toastmessage('showErrorToast', "Поле 'Сообщение' обязательно для заполнения!");
                        } else if (error == 'subject') {
                            $().toastmessage('showErrorToast', "Ваше имя и телефон обязательны для заполнения!");
                        } else if (error == 'sender') {
                            $().toastmessage('showErrorToast', "Введите правильный адрес электронной почты.");
                        } else {
                            $().toastmessage('showWarningToast', "Непредвиденная ошибка");
                        }
                    }
                }
            }
        })
    });
});


(function ($) {
    var settings = {
        inEffect: {opacity: 'show'},	// in effect
        inEffectDuration: 600,				// in effect duration in miliseconds
        stayTime: 3000,				// time in miliseconds before the item has to disappear
        text: '',					// content of the item. Might be a string or a jQuery object. Be aware that any jQuery object which is acting as a message will be deleted when the toast is fading away.
        sticky: false,				// should the toast item sticky or not?
        type: 'notice', 			// notice, warning, error, success
        position: 'top-center',        // top-left, top-center, top-right, middle-left, middle-center, middle-right ... Position of the toast container holding different toast. Position can be set only once at the very first call, changing the position after the first call does nothing
        closeText: '',                 // text which will be shown as close button, set to '' when you want to introduce an image via css
        close: null                // callback function when the toastmessage is closed
    };

    var methods = {
        init: function (options) {
            if (options) {
                $.extend(settings, options);
            }
        },

        showToast: function (options) {
            var localSettings = {};
            $.extend(localSettings, settings, options);

            // declare variables
            var toastWrapAll, toastItemOuter, toastItemInner, toastItemClose, toastItemImage;

            toastWrapAll = (!$('.toast-container').length) ? $('<div></div>').addClass('toast-container').addClass('toast-position-' + localSettings.position).appendTo('body') : $('.toast-container');
            toastItemOuter = $('<div></div>').addClass('toast-item-wrapper');
            toastItemInner = $('<div></div>').hide().addClass('toast-item toast-type-' + localSettings.type).appendTo(toastWrapAll).html($('<p>').append(localSettings.text)).animate(localSettings.inEffect, localSettings.inEffectDuration).wrap(toastItemOuter);
            toastItemClose = $('<div></div>').addClass('toast-item-close').prependTo(toastItemInner).html(localSettings.closeText).click(function () {
                $().toastmessage('removeToast', toastItemInner, localSettings)
            });
            toastItemImage = $('<div></div>').addClass('toast-item-image').addClass('toast-item-image-' + localSettings.type).prependTo(toastItemInner);

            if (navigator.userAgent.match(/MSIE 6/i)) {
                toastWrapAll.css({top: document.documentElement.scrollTop});
            }

            if (!localSettings.sticky) {
                setTimeout(function () {
                        $().toastmessage('removeToast', toastItemInner, localSettings);
                    },
                    localSettings.stayTime);
            }
            return toastItemInner;
        },

        showNoticeToast: function (message) {
            var options = {text: message, type: 'notice'};
            return $().toastmessage('showToast', options);
        },

        showSuccessToast: function (message) {
            var options = {text: message, type: 'success'};
            return $().toastmessage('showToast', options);
        },

        showErrorToast: function (message) {
            var options = {text: message, type: 'error'};
            return $().toastmessage('showToast', options);
        },

        showWarningToast: function (message) {
            var options = {text: message, type: 'warning'};
            return $().toastmessage('showToast', options);
        },

        removeToast: function (obj, options) {
            obj.animate({opacity: '0'}, 600, function () {
                obj.parent().animate({height: '0px'}, 300, function () {
                    obj.parent().remove();
                });
            });
            // callback
            if (options && options.close !== null) {
                options.close();
            }
        }
    };

    $.fn.toastmessage = function (method) {

        // Method calling logic
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.toastmessage');
        }
    };

})(jQuery);


$(document).ready(function () {
    var content = $('#add-order-form2');
    content.submit(function (e) {
        e.preventDefault();
        $.ajax({
            url: "/contact/send/",
            type: 'post',
            data: content.serialize(),
            success: function (data) {
                if (data.status == 'Ok') {
                    $().toastmessage('showSuccessToast', "Ваше сообщение было успешно отправлено");
                }
                if (data.status == 'error') {
                    for (error in data.errors) {
                        console.log(error);
                        if (error == 'message') {
                            $().toastmessage('showErrorToast', "Поле 'Сообщение' обязательно для заполнения!");
                        } else if (error == 'subject') {
                            $().toastmessage('showErrorToast', "Ваше имя и телефон обязательны для заполнения!");
                        } else if (error == 'sender') {
                            $().toastmessage('showErrorToast', "Введите правильный адрес электронной почты.");
                        } else {
                            $().toastmessage('showWarningToast', "Непредвиденная ошибка");
                        }
                    }
                }
            }
        })
    });
});

$(document).ready(function () {
    $(".title-contact, .contact-email").fadeIn("slow");
});