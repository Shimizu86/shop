# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Slider',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('image', models.ImageField(verbose_name='Изображение', upload_to='slider')),
                ('large_comment', models.CharField(verbose_name='Комментарий', max_length=200, blank=True)),
                ('small_comment', models.CharField(verbose_name='Комментарий', max_length=200, blank=True)),
            ],
            options={
                'verbose_name_plural': 'Слайдер изображений',
                'verbose_name': 'Изображение',
            },
        ),
    ]
