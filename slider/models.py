from django.db import models
from PIL import Image, ImageOps


class Slider(models.Model):
    image = models.ImageField(verbose_name='Изображение', upload_to='slider')
    large_comment = models.CharField(verbose_name='Крупный комментарий', max_length=200, blank=True)
    small_comment = models.CharField(verbose_name='Комментарий', max_length=200, blank=True)
    phone = models.BooleanField(verbose_name='Телефон', default=False)

    def __str__(self):
        return self.image.url

    def delete(self, *args, **kwargs):
        storage, path = self.image.storage, self.image.path
        super(Slider, self).delete(*args, **kwargs)
        storage.delete(path)

    def save(self, *args, **kwargs):
        try:
            slide = Slider.objects.get(id=self.id)
            if slide.image != self.image:
                slide.image.delete()
        except:
            pass

        super(Slider, self).save(*args, **kwargs)

        img = Image.open(self.image.path)
        img = ImageOps.fit(img, (1200, 360), Image.ANTIALIAS)
        img.save(self.image.path, quality=100)

    class Meta:
        verbose_name = 'Изображение'
        verbose_name_plural = 'Слайдер изображений'
