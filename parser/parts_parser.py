# coding: utf8

from xlrd import open_workbook
import simplejson as json
import os

json_folder = '/var/www/magazine/shop/fixtures/'
goods_pk = 1
image_pk = 1

book = open_workbook('files/Parts.xls')
sheet = book.sheet_by_index(0)

keys = [sheet.cell(0, col_index).value for col_index in range(sheet.ncols)]
print("keys are", keys)

dict_list = []
for row_index in range(1, sheet.nrows):
    d = {keys[col_index]: sheet.cell(row_index, col_index).value for col_index in range(sheet.ncols)}
    for image in d['images'].split(','):
        image_fieldset = {'part': goods_pk, 'image_url': image}
        dict_list.append({"model": "shop.imagesparts", "fields": image_fieldset, 'pk': image_pk})
        image_pk += 1

    d.pop('images')
    print(d)
    d['cat_num'] = str(d['cat_num'])
    d['original_cat_num'] = str(d['original_cat_num'])
    dict_list.append({"model": "shop.parts", "fields": d, 'pk': goods_pk})

    goods_pk += 1

j = json.dumps(dict_list)

with open(json_folder + 'Goods.json', 'w') as f:
    f.write(j)
