from parser.models import DataFile
from django.contrib import admin
from django import forms


class DataFileForm(forms.ModelForm):
    class Meta:
        model = DataFile
        fields = ('file',)

    def clean_file(self):
        allowed_names = ['Filters', 'Oils', 'Parts']
        if self.cleaned_data['file'].name.split('.')[0] not in allowed_names:
            raise forms.ValidationError("Вы загружаете неверный файл")
        return self.cleaned_data['file']


class DataFileAdmin(admin.ModelAdmin):
    form = DataFileForm


admin.site.register(DataFile, DataFileAdmin)
