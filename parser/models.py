import os
from django.db import models
from xlrd import open_workbook

from shop.models import Oils, Parts, Filters
from django.db.models.loading import get_model


class DataFile(models.Model):
    file = models.FileField(verbose_name='Файл прайса', upload_to='price')

    def __str__(self):
        return self.file.url

    class Meta:
        verbose_name = "Файл загрузки"
        verbose_name_plural = 'Файлы загрузки'

    def delete(self, *args, **kwargs):
        storage, path = self.file.storage, self.file.path
        super(DataFile, self).delete(*args, **kwargs)
        storage.delete(path)

    def save(self, *args, **kwargs):
        super(DataFile, self).save(*args, **kwargs)
        try:
            file_for_parse = open_workbook(self.file.path)
            model_str = os.path.basename(self.file.path).split('.')[0]
            self.delete()
            model = get_model('shop', model_str)
            model.objects.all().delete()
            sheet = file_for_parse.sheet_by_index(0)
            keys = [sheet.cell(0, col_index).value for col_index in range(sheet.ncols)]
            for row_index in range(1, sheet.nrows):
                d = {keys[col_index]: sheet.cell(row_index, col_index).value for col_index in range(sheet.ncols)}
                images = d['images'].split(',')
                d.pop('images')
                if model_str == 'Oils':
                    Oil = Oils.objects.create(**d)
                    for image in images:
                        Oil.imagesoils_set.create(image_url=image)
                elif model_str == 'Parts':
                    Part = Parts.objects.create(**d)
                    for image in images:
                        Part.imagesparts_set.create(image_url=image)
                elif model_str == 'Filters':
                    Filter = Filters.objects.create(**d)
                    for image in images:
                        Filter.imagesfilters_set.create(image_url=image)
        except:
            pass
