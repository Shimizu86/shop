from django.db import models
from shop.choices import *


class Parts(models.Model):
    name = models.CharField(max_length=200, verbose_name='Наименование')
    cat_num = models.CharField(max_length=200, verbose_name='Каталожный номер')
    price = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Цена')
    applicability = models.CharField(max_length=300, blank=True, verbose_name='Применяемость')
    original_cat_num = models.CharField(max_length=200, blank=True, verbose_name='Оригинальный номер', default='')
    brand = models.CharField(max_length=100, blank=True, verbose_name='Бренд', default='')
    amount = models.IntegerField(blank=True, default='1', verbose_name='В наличии')

    class Meta:
        verbose_name = "Запчасть"
        verbose_name_plural = 'Запчасти'


class Oils(models.Model):
    name = models.CharField(max_length=200, verbose_name='Наименование')
    cat_num = models.CharField(max_length=200, verbose_name='Каталожный номер')
    price = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Цена')
    brand = models.CharField(max_length=100, blank=True, verbose_name='Бренд', default='')
    amount = models.IntegerField(blank=True, default='1', verbose_name='В наличии')
    viscosity = models.CharField(max_length=10, choices=OIL_VISCOSITY_CHOICES, verbose_name='Вязкость')
    oil_type = models.CharField(max_length=20, choices=OIL_TYPE_CHOICES, verbose_name='Тип масла')
    size = models.IntegerField(default=1, verbose_name='Объем')

    class Meta:
        verbose_name = "Масло"
        verbose_name_plural = 'Масла'


class Filters(models.Model):
    name = models.CharField(max_length=200, verbose_name='Наименование')
    cat_num = models.CharField(max_length=200, verbose_name='Каталожный номер')
    price = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Цена')
    brand = models.CharField(max_length=100, blank=True, verbose_name='Бренд', default='')
    size = models.CharField(max_length=200, blank=True, verbose_name='Размеры')
    amount = models.IntegerField(blank=True, default='1', verbose_name='В наличии')
    applicability = models.CharField(max_length=300, blank=True, verbose_name='Применяемость')

    class Meta:
        verbose_name = "Фильтр"
        verbose_name_plural = 'Фильтра'


class ImagesParts(models.Model):
    part = models.ForeignKey(Parts)
    image_url = models.URLField(max_length=500, verbose_name='URL изображения')


class ImagesOils(models.Model):
    oil = models.ForeignKey(Oils)
    image_url = models.URLField(max_length=500, verbose_name='URL изображения')


class ImagesFilters(models.Model):
    oil = models.ForeignKey(Filters)
    image_url = models.URLField(max_length=500, verbose_name='URL изображения')


class Task(models.Model):
    user = models.CharField(max_length=200)
    vin = models.CharField(max_length=100)
    text = models.CharField(max_length=2000)
    pub_date = models.DateTimeField(auto_now_add=True)
    mail = models.CharField(max_length=100)
    resolved = models.NullBooleanField(verbose_name='Решено', default=None)

    class Meta:
        verbose_name = "Заявка"
        verbose_name_plural = 'Заявки'


class Client(models.Model):
    name = models.CharField(max_length=200, verbose_name='Имя')
    phone = models.CharField(max_length=20, verbose_name='Телефон')
    car_info = models.CharField(max_length=500, blank=True, verbose_name='Данные об а/м')
    buyed_part = models.CharField(max_length=500, blank=True, verbose_name='Купленные з/ч')
    comment = models.CharField(max_length=1000, blank=True, verbose_name='Комментарий')

    class Meta:
        verbose_name = "Клиент"
        verbose_name_plural = 'Клиенты'


class VinNum(models.Model):
    client = models.ForeignKey(Client)
    vin_num = models.CharField(max_length=18, verbose_name='Вин номер')
