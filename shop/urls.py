from django.conf.urls import url

from shop.views import FullCatalogView, CatalogPartsView, CatalogOilsView, CatalogFiltersView, get_part_info, get_oil_info, get_search_result, \
    get_filter_info

urlpatterns = [
    url(r'^$', FullCatalogView.as_view(), name='catalog'),
    url(r'^parts/$', CatalogPartsView.as_view(), name='catalog_parts'),
    url(r'^oils/$', CatalogOilsView.as_view(), name='catalog_oils'),
    url(r'^filters/$', CatalogFiltersView.as_view(), name='catalog_filters'),
    url(r'^getpartinfo/', get_part_info, name='get_part_info'),
    url(r'^getoilinfo/', get_oil_info, name='get_oil_info'),
    url(r'^getfilterinfo/', get_filter_info, name='get_filter_info'),
    url(r'^search/', get_search_result, name='search'),
]
