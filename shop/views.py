# coding: utf-8
from django.db.models import Q
from django.http import HttpResponseServerError, HttpResponse
from django.shortcuts import render
from django.template.loader import render_to_string
from django.views.generic import ListView, FormView

from contact.forms import ContactForm
from contact.models import Contact
from shop.models import Parts, Oils, Filters
from slider.models import Slider


class MainView(FormView):
    model = Contact
    form_class = ContactForm
    context_object_name = 'user_list'
    template_name = 'main.html'
    success_url = '#'

    def get_context_data(self, **kwargs):
        context = super(MainView, self).get_context_data(**kwargs)
        context['contacts'] = Contact.objects.all()
        context['slider'] = Slider.objects.all()
        return context


class BaseCatalogView(FormView):
    form_class = ContactForm
    success_url = '#'
    template_name = 'shop.html'

    def get_context_data(self, **kwargs):
        context = super(BaseCatalogView, self).get_context_data(**kwargs)
        context['slider'] = Slider.objects.all()
        context['contacts'] = Contact.objects.all()
        return context


class FullCatalogView(BaseCatalogView):
    model = Parts
    context_object_name = 'parts_list'

    def get_context_data(self, **kwargs):
        context = super(FullCatalogView, self).get_context_data(**kwargs)
        context['parts_list'] = Parts.objects.all()
        context['oils_list'] = Oils.objects.all()
        context['filters_list'] = Filters.objects.all()
        return context


class CatalogPartsView(BaseCatalogView):
    model = Parts
    context_object_name = 'parts_list'

    def get_context_data(self, **kwargs):
        context = super(CatalogPartsView, self).get_context_data(**kwargs)
        context['parts_list'] = Parts.objects.all()
        return context


class CatalogOilsView(BaseCatalogView):
    model = Oils
    context_object_name = 'oils_list'

    def get_context_data(self, **kwargs):
        context = super(CatalogOilsView, self).get_context_data(**kwargs)
        context['oils_list'] = Oils.objects.all()
        return context


class CatalogFiltersView(BaseCatalogView):
    model = Filters
    context_object_name = 'filters_list'

    def get_context_data(self, **kwargs):
        context = super(CatalogFiltersView, self).get_context_data(**kwargs)
        context['filters_list'] = Filters.objects.all()
        return context


def get_part_info(request):
    if request.is_ajax():
        part_id = int(request.GET.get('id'))
        filtered_part = Parts.objects.get(id=part_id)
        html = render_to_string('modal-part.html', {'part': filtered_part})
        return HttpResponse(html)
    else:
        raise HttpResponseServerError


def get_filter_info(request):
    if request.is_ajax():
        filter_id = int(request.GET.get('id'))
        filtered_filter = Filters.objects.get(id=filter_id)
        html = render_to_string('modal-filter.html', {'filter': filtered_filter})
        return HttpResponse(html)
    else:
        raise HttpResponseServerError


def get_oil_info(request):
    if request.is_ajax():
        oil_id = int(request.GET.get('id'))
        filtered_oil = Oils.objects.get(id=oil_id)
        html = render_to_string('modal-oil.html', {'oil': filtered_oil})
        return HttpResponse(html)
    else:
        raise HttpResponseServerError


def get_search_result(request):
    slider = Slider.objects.all()
    form = ContactForm
    if 'search' in request.GET:
        sr = str(request.GET.get('search'))
        parts_list = Parts.objects.filter(
            Q(name__icontains=sr) | Q(brand__icontains=sr) | Q(cat_num__icontains=sr) | Q(
                original_cat_num__icontains=sr) | Q(applicability__icontains=sr)
        )
        oils_list = Oils.objects.filter(
            Q(name__icontains=sr) | Q(brand__icontains=sr) | Q(cat_num__icontains=sr) | Q(viscosity__icontains=sr) | Q(
                oil_type__icontains=sr)
        )
        filters_list = Filters.objects.filter(
            Q(name__icontains=sr) | Q(brand__icontains=sr) | Q(cat_num__icontains=sr) | Q(applicability__icontains=sr)
        )
        return render(request, 'shop.html', {'sr': sr,
                                             'parts_list': parts_list,
                                             'oils_list': oils_list,
                                             'filters_list': filters_list,
                                             'slider': slider,
                                             'form': form,
                                             })
