def choice_maker(choice_list):
    choices = ((ch, ch) for ch in choice_list)
    return choices


OIL_VISCOSITY = ('0w30', '0w40', '5w30', '5w40', '10w30', '10w40')
OIL_TYPES = ('Синтетическое', 'Полусинтетическое', 'Минеральное')

OIL_VISCOSITY_CHOICES = choice_maker(OIL_VISCOSITY)
OIL_TYPE_CHOICES = choice_maker(OIL_TYPES)
