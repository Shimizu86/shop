# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Goods',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('name', models.CharField(max_length=200)),
                ('cat_num', models.CharField(max_length=50)),
                ('image', models.ImageField(verbose_name='Изображение', upload_to='goods')),
                ('price', models.DecimalField(verbose_name='Цена', decimal_places=2, max_digits=10)),
                ('applicability', models.CharField(blank=True, max_length=300)),
            ],
        ),
    ]
