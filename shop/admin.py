from django import forms
from django.contrib import admin
from shop.models import ImagesParts, ImagesOils, Parts, Oils, Task, ImagesFilters, Filters, VinNum, Client


class ImagesPartsInLine(admin.StackedInline):
    model = ImagesParts
    extra = 3


class ImagesOilsInLine(admin.StackedInline):
    model = ImagesOils
    extra = 3


class ImagesFiltersInLine(admin.StackedInline):
    model = ImagesFilters
    extra = 3


class PartsAdmin(admin.ModelAdmin):
    inlines = [ImagesPartsInLine]
    list_display = ('name', 'cat_num', 'original_cat_num', 'price')
    search_fields = ['name', 'cat_num', 'original_cat_num']


class OilsAdmin(admin.ModelAdmin):
    inlines = [ImagesOilsInLine]
    list_display = ('name', 'cat_num', 'viscosity', 'oil_type', 'price')
    search_fields = ['name', 'cat_num', 'viscosity', 'oil_type']


class FiltersAdmin(admin.ModelAdmin):
    inlines = [ImagesFiltersInLine]
    list_display = ('name', 'cat_num', 'price')
    search_fields = ['name', 'cat_num']


class TasksAdmin(admin.ModelAdmin):
    list_display = ('user', 'pub_date', 'vin', 'text', 'mail', 'resolved')
    search_fields = ['user', 'pub_date', 'vin', 'text', 'mail']


class ClientVinInLine(admin.StackedInline):
    model = VinNum
    extra = 1


class CarInfoAdminForm(forms.ModelForm):
    class Meta:
        model = Client
        fields = ('name', 'phone', 'car_info', 'buyed_part', 'comment')
        widgets = {
            'car_info': forms.Textarea(attrs={'cols': 100, 'rows': 8}),
            'comment': forms.Textarea(attrs={'cols': 100, 'rows': 5}),
            'buyed_part': forms.Textarea(attrs={'cols': 100, 'rows': 8}),
        }


class ClientsAdmin(admin.ModelAdmin):
    form = CarInfoAdminForm
    inlines = [ClientVinInLine]
    list_display = ('name', 'phone', 'car_info')
    search_fields = ['name', 'phone', 'car_info', 'comment']


admin.site.register(Task, TasksAdmin)
admin.site.register(Parts, PartsAdmin)
admin.site.register(Filters, FiltersAdmin)
admin.site.register(Oils, OilsAdmin)
admin.site.register(Client, ClientsAdmin)
